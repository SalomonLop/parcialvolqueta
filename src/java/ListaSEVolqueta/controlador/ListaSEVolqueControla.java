/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ListaSEVolqueta.controlador;

import ListaSEVolquetas.modelo.Nodo;
import ListaSEVolquetas.modelo.Volqueta;

/**
 *
 * @author marlon
 */
public class ListaSEVolqueControla {
    private ListaSEVolqueta lista= new ListaSEVolqueta();
     
    private Nodo temp;
    
    private boolean verNuevo=false;
    
    private boolean verBuscar=false;
    
    private Volqueta volquetaAdicionar;
    
    private Volqueta volquetaEncontrado;
    
    private int posicionBuscar=0;

    public int getPosicionBuscar() {
        return posicionBuscar;
    }

    public void setPosicionBuscar(int posicionBuscar) {
        this.posicionBuscar = posicionBuscar;
    }
    
    

    public Volqueta getEstudianteEncontrado() {
        return volquetaEncontrado;
    }

    public void setEstudianteEncontrado(Volqueta estudianteEncontrado) {
        this.volquetaEncontrado= estudianteEncontrado;
    }
    
    

    public boolean isVerBuscar() {
        return verBuscar;
    }

    public void setVerBuscar(boolean verBuscar) {
        this.verBuscar = verBuscar;
    }

    
    
    public Volqueta getEstudianteAdicionar() {
        return volquetaAdicionar;
    }

    public void setEstudianteAdicionar(Volqueta estudianteAdicionar) {
        this.volquetaAdicionar = estudianteAdicionar;
    }
    
    
    public void mostrarListado()
    {
        listado= lista.listarNodos();
    }

    public boolean isVerNuevo() {
        return verNuevo;
    }

    public void setVerNuevo(boolean verNuevo) {
        this.verNuevo = verNuevo;
    }
    
    

    public Nodo getTemp() {
        return temp;
    }

    public void setTemp(Nodo temp) {
        this.temp = temp;
    }
    
    
    
    private String listado;

    public String getListado() {
        return listado;
    }

    public void setListado(String listado) {
        this.listado = listado;
    }
    
    
    
    public ListaSEVolqueta getLista() {
        return lista;
    }

    public void setLista(ListaSEVolqueta lista) {
        this.lista = lista;
    }
    
    
     
     public void irAlsiguiente()
     {
         if(temp.getSiguiente()!=null)
         {
             temp=temp.getSiguiente();
         }
     }
     
     
     
     
     public void irAlPrimero()
     {
         temp=lista.getCabeza();
     }
     
     
      public void irAlUltimo()
     {
         Nodo ultimo= lista.obtenerUltimoNodo();
         if(ultimo!=null)
         {
            temp=ultimo;
         }   
     }
     public void verCrearEstudiante()
     {
         verNuevo=true;
         volquetaAdicionar= new Volqueta();
     }
     
     public void adicionarAlInicio()
     {
         lista.adicionarNodoAlInicio(volquetaAdicionar);
         irAlPrimero();
         verNuevo=false;
     }
     
      public void adicionarAlFinal()
     {
         lista.adicionarAlFinal();
         irAlPrimero();
         verNuevo=false;
     }
      
      
     
}
