
import ListaSEVolqueta.controlador.ListaSEVolqueta;
import ListaSEVolquetas.modelo.Nodo;
import ListaSEVolquetas.modelo.Volqueta;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author marlon
 */


@Named(value = "beanVolquetas")
@SessionScoped
public class BeanVolquetas implements Serializable {
    
    private boolean deshabilitarNuevo=true;

    
    private ListaSEVolqueta listaSE= new ListaSEVolqueta ();
    
    private Nodo volquetaMostrar= new Nodo(new Volqueta());
    
    private boolean  verTabla=false;

    public boolean isVerTabla() {
        return verTabla;
    }

    public void setVerTabla(boolean verTabla) {
        this.verTabla = verTabla;
    }
    
    
    
    /**
     * Creates a new instance of BeanJardinInfantil
     */
    public BeanVolquetas() {
        
    }
    @PostConstruct
    public void llenarVolquetas()
    {
        listaSE.adicionarNodoAlInicio(new Volqueta());
        listaSE.adicionarNodoAlInicio(new Volqueta());
        listaSE.adicionarNodoAlInicio(new Volqueta());
        listaSE.adicionarNodoAlInicio(new Volqueta());
        irAlPrimero();
    }

    

    
    
    public boolean isDeshabilitarNuevo() {
        return deshabilitarNuevo;
    }

    public void setDeshabilitarNuevo(boolean deshabilitarNuevo) {
        this.deshabilitarNuevo = deshabilitarNuevo;
    }
    
    
    public ListaSEVolqueta getListaSE() {
        return listaSE;
    }

    public void setListaSE(ListaSEVolqueta listaSE) {
        this.listaSE = listaSE;
    }

    public Nodo getVolquetaMostrar() {
     
        return volquetaMostrar;
    }

    public void setInfanteMostrar(Nodo infanteMostrar) {
        this.volquetaMostrar= volquetaMostrar;
    }

    
    
    
    public void habilitarCrearVolquetas()
    {
        deshabilitarNuevo=false;
        volquetaMostrar= new Nodo(new Volqueta());
    }
    
    public void guardarVolquetaAlFinal()
    {
        listaSE.adicionarNodoAlfinal(VolquetaMostrar());
        volquetaMostrar= new Nodo(new Volqueta());
        deshabilitarNuevo=true;
        irAlPrimero();
    }
    
    public void guardarVolquetaAlInicio()
    {
        listaSE.adicionarNodoAlInicio(volquetaMostrar.getDato());
      volquetaMostrar  = new Nodo(new Volqueta());
        deshabilitarNuevo=true;
        irAlPrimero();
        
    }
    
    
    public void  irAlPrimero()
    {
        
       volquetaMostrar=listaSE.getCabeza();
        
    }
    
    
    public void irAlSiguiente()
    {
        if(volquetaMostrar.getSiguiente()!=null)
        {
            volquetaMostrar= volquetaMostrar.getSiguiente();
        }    
    }
    
    public void irAlUltimo()
    {
         
    }
    
    
 
      public void modificarLista()
   {
       
       listaSE.modificarLista();
       
   } 
      
   public void cancelarGuardado()
   {
       deshabilitarNuevo=true;
       irAlPrimero();
   }
   
   public void visualizarTabla()
   {
       verTabla=true;
   }
   
  public void  calVolqueCapaMayor (){
   }
  public void calcularVolquetaPromedioCapacidad(){
      
  }
   
   public void eliminarVolqueta()
   {
       listaSE.eliminarNodo(volquetaMostrar.getDato());
       irAlPrimero();
   }

    private Object VolquetaMostrar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
